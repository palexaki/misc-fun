#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct node {
  int val;
  struct node *next;
} NODE;


NODE* newelement(int value) {
  NODE *newel = (NODE*)malloc(sizeof(NODE));
  newel->val = value;
  newel->next = NULL;
  return newel;
}

NODE* addend(NODE *head, NODE *new) {
  NODE* el;

  if (head == NULL) {
    return new;
  }

  for (el = head; el->next != NULL; el = el->next)
    ;

  el->next = new;
  return head;
}

NODE* push(NODE *head, NODE *new) {
  new->next = head;
  return new;
}



NODE* addordered(NODE *head, NODE *new) {
  if (head == NULL) {
    return new;
  }

  NODE *el1, *el2;
  el1 = el2 = head;

  while (1) {

    if (el2->val > new->val) {
      if (el2 == head) {
        return push(head, new);
      } else {
        el1->next = new;
        new->next = el2;
        return head;
      }
    }

    if (el2->next == NULL) {
      return addend(head, new);
    }
    el1 = el2;
    el2 = el2->next;
  }
}

NODE* deleteelement(NODE *head, int val) {
  NODE *el1, *el2;

  el1 = NULL;

  for (el2 = head; el2 != NULL; el2 = el2->next) {
    if (el2->val == val) {
      if (el2 == head) {
        head = head->next;
      } else {
        el1->next = el2->next;
      }
      free(el2);
      return head;
    }
    el1 = el2;
  }
  return head;
}

NODE *deleteindex(NODE *head, int index) {

  NODE *prev, *curr;
  prev = NULL;
  curr = head;
  for (int i = 0; i < index; i++) {
    prev = curr;
    curr = curr->next;
  }

  if (prev != NULL) {
    prev->next = curr->next;
  } else {
    head = head->next;
  }

  free(curr);
  return head;
}

void deletelist(NODE *head) {
  NODE *temp;
  while (head != NULL) {
    temp = head;
    head = head->next;
    free(temp);
  }
}

int count(NODE *head, int val) {
  int res = 0;
  for (NODE *curr = head; curr != NULL; curr=curr->next) {
    if (curr->val == val) {
      res++;
    }
  }

  return res;
}

int indexof(NODE *head, int val) {
  int i = 0;
  for (NODE *curr;curr != NULL; curr = curr->next) {
    if (curr->val == val) {
      return i;
    }
    i++;
  }
  return -1;
}

NODE* reverse(NODE *head) {
  NODE *prev = NULL, *curr = head, *next;

  while (curr != NULL) {
    next = curr->next;
    curr->next = prev;
    prev = curr;
    curr = next;
  }
  return prev;
}

void printlist(NODE *head) {
  NODE *curr = head;
  while (curr != NULL) {
    printf("%d  ", curr->val);
    curr = curr->next;
  }
  printf("\n");
}

NODE *map(NODE *head, int (*funcp)(int)) {
  NODE *curr = head;
  while (curr != NULL) {
    curr->val = funcp(curr->val);
    curr = curr->next;
  }
  return head;
}

int reduce(NODE *head, int (*funcp)(int, int)) {
  NODE *next = head->next;
  int res = funcp(head->val, next->val);
  next = next->next;
  while (next != NULL) {
    res = funcp(res, next->val);
    next = next->next;
  }

  return res;
}

NODE *copy (NODE *source) {
  NODE *res = NULL, *curr = source;

  while (curr != NULL) {
    res = addend(res, newelement(curr->val));
    curr = curr->next;
  }

  return res;

}

NODE *substitute(int new, int old, NODE *head) {
  NODE *curr = head;

  while (curr!=NULL) {
    if (curr->val == old) {
      curr->val = new;
    }
    curr = curr->next;
  }

  return head;
}

NODE *filter(NODE *head, int (*predicate)(int)) {
  NODE *prev = NULL, *curr = head;

  while (curr!=NULL) {
    if (!predicate(curr->val)) {
      curr = curr->next;
      if (prev==NULL) {
        head = curr;
        continue;
      }

      prev->next = curr;
      continue;
    }
    prev = curr; curr = curr->next;
  }

  return head;
}

int get(NODE *head, int index) {
  NODE *curr = head;
  for (int i = 0; i < index; i++) {
    curr = curr->next;
  }

  return curr->val;
}

NODE *getnode(NODE *head, int index) {
  NODE *curr = head;
  for (int i = 0; i < index; i++) {
    curr = curr->next;
  }

  return curr;
}

int length(NODE *head) {
  int res = 0;
  NODE *curr = head;

  while (curr != NULL) {
    res++;
    curr = curr->next;
  }

  return res;
}

NODE *append(NODE *begin, NODE *end) {
  NODE *curr = begin;
  while (curr->next != NULL) {
    curr = curr->next;
  }

  curr->next = end;
  return begin;
}

int sorted(NODE *head, int (*compare)(int, int)) {
  NODE *curr = head;

  while (curr->next != NULL) {
    if (compare(curr->val, curr->next->val) > 0) {
      return 0;
    }
    curr = curr->next;
  }

  return 1;
}

NODE *bubblesort(NODE *head, int (*compare)(int, int)) {
  int pass = 0;
  while (!sorted(head, compare)) {
    NODE *prev = NULL, *curr = head, *next = head->next;
    while (next != NULL) {
      if (compare(curr->val, next->val) > 0) {
        if (prev != NULL) {
          prev->next = next;
        } else {
          head = next;
        }
        curr->next = next->next;
        next->next = curr;
        NODE *temp = curr;
        curr = next;
        next = curr;
      }
      prev = curr;
      curr = curr->next;
      next = curr->next;
    }
  }

  return head;
}

NODE *mergesort(NODE *head, int (*compare)(int, int)) {
  int len = length(head);
  if (len <= 1) {
    return head;
  }

  NODE *last = getnode(head, len/2 - 1);
  NODE *second = last->next;
  last->next = NULL;

  NODE *first = mergesort(head, compare);
  second = mergesort(second, compare);

  NODE *new = NULL;

  while ((first != NULL) || (second != NULL)) {
    if (first == NULL) {
      new = addend(new, newelement(second->val));
      second = second->next;
    } else if (second == NULL) {
      new = addend(new, newelement(first->val));
      first = first->next;
    } else if (compare(first->val, second->val) <= 0) {
      new = addend(new, newelement(first->val));
      first = first->next;
    } else {
      new = addend(new, newelement(second->val));
      second = second->next;
    }
  }

  return new;
}

NODE *shuffle(NODE *head) {
  NODE *new = NULL;
  NODE *items_left = NULL;
  int ilen = length(head);
  int new_index;

  while (ilen != 0) {
    new_index =rand()%ilen;
    NODE *new_node = newelement(get(head, new_index));
    new = addend(new, new_node);
    head = deleteindex(head, new_index);
    ilen--;
  }

  return new;
}

int square(int x) {
  return x * x;
}

int sum(int x, int y) {
  return x + y;
}

int odd(int x) {
  return x%2;
}

int smaller(int x, int y) {
  return x - y;
}

int main() {

  printf("Placeholder");
}
